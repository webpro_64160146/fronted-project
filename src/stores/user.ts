import type User from "@/types/User";
import { defineStore } from "pinia";
import { ref } from "vue";
import userService from "@/services/user";
export const useUserStore = defineStore("User", () => {
  const users = ref<User[]>([]);
  const editedProduct = ref<User>({ name: "", age: 0, tel: "", gender: "" });
  async function getUser() {
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  return { getUser, editedProduct, users };
});
