export default interface User {
  id?: number;
  name: string;
  age: number;
  tel: string;
  gender: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
